// 导入组件
import Login from '../pages/login';

// 导出路由
export default [
    {
        path: '/login',
        title: '登录',
        component: Login
    }
]