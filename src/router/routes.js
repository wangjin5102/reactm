// 导入路由文件
import navs from './navs'
import login from './login'
import config from './config'

// 合并导出
export default [
    ...navs,
    ...login,
    ...config,
];