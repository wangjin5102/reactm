// 导入组件
import Err404 from '../components/err/404';
import Err500 from '../components/err/500';

// 导出路由
export default [
    {
        path: '/500',
        component: Err500
    },
    {
        path: '*',
        component: Err404
    }
]