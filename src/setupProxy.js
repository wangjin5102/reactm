const proxy = require('http-proxy-middleware')

module.exports = function (app) {

    app.use(
        proxy('/api', {
            
            target: "http://ustbhuangyi.com", // 目标服务器网址
            changeOrigin: true, // 是否允许跨域
            secure: false,      // 关闭SSL证书验证HTTPS接口需要
            pathRewrite: {      // 重写路径请求
                '^/api': ''
                // 重写
                // pathRewrite: {'^/old/api' : '/new/api'}
                // 移除
                // pathRewrite: {'^/remove/api' : ''}
                // 添加
                // pathRewrite: {'^/' : '/basepath/'}
            },
        }),
        proxy('/adc', {
            target: "https://c.y.qq.com", // 目标服务器网址
            changeOrigin: true, // 是否允许跨域
            secure: false,      // 关闭SSL证书验证HTTPS接口需要
            pathRewrite: {      // 重写路径请求
                '^/adc': ''
                // 重写
                // pathRewrite: {'^/old/api' : '/new/api'}
                // 移除
                // pathRewrite: {'^/remove/api' : ''}
                // 添加
                // pathRewrite: {'^/' : '/basepath/'}
            },
        })
    );

    // ...

};