import React, { Component } from 'react'

// styled样式

import { HeaderDiv} from './styled'
class HeaderIndex extends Component {
    render() {
        return (
            <HeaderDiv>
                <img src="/logo192.png" alt="logo"></img>
                <span>菜鸡云音乐</span>
            </HeaderDiv>
        )
    }
}
export default HeaderIndex
