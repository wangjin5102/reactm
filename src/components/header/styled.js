import styled from 'styled-components'

export const HeaderDiv = styled.div`
    width:100%;
    height:44px;
    display:flex;
    justify-content: center;
    align-items: center;
    img{
        width:30px;
        height:30px;
        margin-right:8px;
    }
    span{
        font-size:18px;
        color:#ffcd32;
    }
`