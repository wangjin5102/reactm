import styled from 'styled-components'

export const FootDiv = styled.div`
    width:100%;
    height:60px;
    background:#333;
    position:fixed;
    bottom:0;
    z-index:9;
    display: flex;
    justify-content: space-around;
    align-items: center;
    .appImage{
        width: 40px;
        height: 40px;
        img{
            width: 40px;
            height: 40px;
            border-radius: 50%;
        }
        // 转动 动画
        img {
            animation: imgrotate 6s linear infinite;}
        }    
        // @keyframes imgrotate
        // {       
        //     0%{transform:rotate(0deg);}
        //     50%{transform:rotate(180deg);}
        //     100%{transform:rotate(360deg);}
        // }
    }
    .appText{
        width: 2.05rem;
        h3{
            margin-bottom: 2px;
            text-overflow: ellipsis;
            overflow: hidden;
            white-space: nowrap;
            font-size: 14px;
            color: #fff;
        }
        p{
            text-overflow: ellipsis;
            overflow: hidden;
            white-space: nowrap;
            font-size: 12px;
            color: hsla(0,0%,100%,.3);
        }
    }
`