import {fromJS} from 'immutable'
import * as constants from './actionTypes'

const broadcastState = fromJS({
    isShowFoot:false
})
export default (state = broadcastState, action) => {
    switch(action.type)
    {
        case constants.ISSHOWFOOT:
            return state.set('isShowFoot', action.data)
        default:
            return state
    }
}
