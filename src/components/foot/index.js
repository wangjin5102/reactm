import React, { Component } from 'react'
import { connect } from 'react-redux';
// 引入仓库数据
// import {
//     isShowBfy
// } from './store/actionCreators'
// styled样式

import { FootDiv } from './styled'

// 导入组件
import {
    Icon,
} from 'antd-mobile';

class Foot extends Component {
    
    render() {
        console.log(this.props.isShowFootData)
        return (
            <FootDiv style={(this.props.isShowFootData.isShowFoot && !this.props.isShowBfyData) ? { display: "flex" } : {display:"none"}}>
                <div className="appImage" onClick={() => { console.log(this.props.isShowFootData)}}>
                    <img src="https://y.gtimg.cn/music/photo_new/T002R300x300M00000459X8Y1wfcQ7.jpg?max_age=2592000" alt="img" />
                </div>
                <div className="appText">
                    <h3>All Falls Down</h3>
                    <p>Alan Walker/Noah Cyrus/Digital Farm Animals/Juliander</p>
                </div>
                <div className="appOpen">
                    <Icon type='check-circle-o' size='lg' style={{ color: "rgba(255, 205, 49, .5)" }} />
                </div>
                <div className="appControl">
                    <Icon type='up' size='lg' style={{ color: "rgba(255, 205, 49, .5)" }} />
                </div>
            </FootDiv>
        )
    }

}

// 高阶组件
const mapStateToProps = state => {    //state为仓库数据

    return {
        isShowFootData: state.toJS().foot,
        isShowBfyData: state.toJS().bfy.isShowBfy
    }
}

const mapDispatchToProps = dispatch => {
    return {
        // changerIsShowBfyFn: params => dispatch(isShowBfy(params)),
        
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Foot)
