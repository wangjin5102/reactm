import styled from 'styled-components'

export const NavsDiv = styled.div`
    width:100%;
    height:44px;
    display:flex;
    justify-content: space-around;
    align-items: center;
    font-size:18px;
    a{
        flex:1;
        text-align:center;
        color:hsla(0,0%,100%,.5);
    }
    a.active{
        color:#ffcd32;
    }
    a.active span{
        border-bottom:2px solid #ffcd32;
    }
`