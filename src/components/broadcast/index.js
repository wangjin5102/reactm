import React, { Component } from 'react'
import { connect } from 'react-redux';
// 引入仓库数据
import {
    isShowBfy
} from './store/actionCreators'
import { isShowFootAction } from '../foot/store/actionCreators'
// styled样式
import { BroadcastDiv } from './styled'
// 引入组件
import Foot from "../foot"
// antdM引入
import {
    Icon,
    Carousel,
    WingBlank,
    Slider,
    WhiteSpace
} from 'antd-mobile';

class Broadcast extends Component {
    constructor(props) {
        super(props)
        this.state = {
            iconStyle: {
                xunhuan: "check-circle-o",
                bofang: "check-circle-o",
                zanting: "cross-circle",
                xihuan: "check-circle-o"
            },
            cdClassName: "",
        }
    }
    log() {
        return (value) => {
            let time = this.refs.audio.duration * value / 30

            this.refs.audio.currentTime = time

        };
    }
    // 播放
    broadcastFn() {
        let icons = this.state.iconStyle.bofang === "check-circle-o" ? "cross-circle" : "check-circle-o"
        let { iconStyle, cdClassName } = this.state
        iconStyle.bofang = icons
        this.setState({
            iconStyle,
            cdClassName,
        })
        if (iconStyle.bofang === "cross-circle") {
            cdClassName = "cdRun"
            this.refs.audio.play()
            this.setState({
                iconStyle,
                cdClassName,
            })
        } else {
            this.refs.audio.pause()
            cdClassName = ""
            this.setState({
                iconStyle,
                cdClassName,
            })
        }

    }
    // 收藏
    xihuanFn() {
        let icons = this.state.iconStyle.xihuan === "check-circle-o" ? "check-circle" : "check-circle-o"
        let iconStyle = this.state.iconStyle
        iconStyle.xihuan = icons
        this.setState({
            iconStyle
        })
    }
    // 循环
    xunhuanFn() {
        let icons = ["check-circle-o", "ellipsis", "loading"]
        let iconStyle = this.state.iconStyle
        for (let i = 0; i < icons.length; i++) {
            if (iconStyle.xunhuan === icons[i]) {
                if (i < 2) {
                    iconStyle.xunhuan = icons[i + 1]
                    console.log(icons[i + 1])
                    this.setState({
                        iconStyle
                    })
                    break;
                } else {
                    iconStyle.xunhuan = icons[0]
                    this.setState({
                        iconStyle
                    })
                    break;
                }
            }
        }
    }
    // 上一首
    upFn() {
    }
    // 下一首
    downFn() {

    }
    // 关闭播放页
    closeBfy() {
        this.props.changerIsShowBfyFn(false)
        this.props.changeIsShowFootFn(true)
    }
    render() {
        console.log(this.props.singerData)
        return (
            <BroadcastDiv >
                <div className="gen" style={this.props.isShowBfyData.isShowBfy ? { display: "block" } : { display: "none" }}>
                    <div className="bj"></div>
                    
                    <div className="main">
                        <Icon type="down" size="lg" onClick={this.closeBfy.bind(this)} />
                        <div className="header">
                            
                                
                                <span>{this.props.isShowBfyData.bfyData.songname}</span>
                            

                            {/* <h2>{this.props.singerData.name}</h2> */}
                            {this.props.singerData.map((item, index) => { 
                                return <h2>{item.name}</h2>
                            })}
                        </div>
                        <WingBlank>
                            <Carousel className="my-carousel"
                                dots
                                dragging={false}
                                swiping
                                autoplay={false}
                                infinite={false}
                                dotActiveStyle={
                                    {
                                        width: "20px", borderRadius: "4px", background: "#ccc"
                                    }
                                }
                                dotStyle={{ background: "#888" }}
                            >
                                <div className="cd v-item">
                                    <div className="cdRoll">
                                        <img src={`https://y.gtimg.cn/music/photo_new/T002R300x300M000${this.props.isShowBfyData.bfyData.albummid}.jpg?max_age=2592000`}  alt="1" />
                                    </div>
                                    <div className="songWord">
                                        <WingBlank>
                                            <Carousel className="my-carousel"
                                                vertical
                                                dots={false}
                                                dragging={false}
                                                swiping={false}
                                                autoplay
                                                infinite
                                            >
                                                <div className="v-item">carousel 1</div>
                                                <div className="v-item">carousel 2</div>
                                                <div className="v-item">carousel 3</div>
                                            </Carousel>
                                        </WingBlank>
                                    </div>
                                </div>
                                <div className="v-item">
                                    as
                            </div>
                            </Carousel>
                        </WingBlank>
                        <div className="controls">
                            <div className="am-slider-example">

                                <WhiteSpace size="lg" />

                                <WingBlank size="lg">
                                    <span style={{ position: "relative", left: "7px" }}>2:30</span>
                                    <Slider
                                        style={{ margin: "auto", width: "2.4rem" }}
                                        handleStyle={{ width: "16px", height: "16px", background: "#ffcd32", position: "relative", bottom: "7px", border: "#fff solid 2px" }}
                                        railStyle={{ background: "rgba(0, 0, 0, .3)", height: "4px" }}
                                        trackStyle={{ background: "#ffcd32", height: "4px" }}
                                        defaultValue={0}
                                        min={0}
                                        max={30}
                                        onChange={this.log.bind(this)}
                                        onAfterChange={this.log('afterChange')}
                                    />
                                    <span style={{ float: "right", position: "relative", bottom: "22px", right: "20px" }}>4:21</span>
                                </WingBlank>
                                <WhiteSpace size="lg" />
                            </div>
                            <div className="btns">
                                <button onClick={this.xunhuanFn.bind(this)}>
                                    <Icon type={this.state.iconStyle.xunhuan} style={{ width: "30px", height: "30px" }} />
                                </button>
                                <button onClick={this.upFn.bind(this)}>
                                    <Icon type="left" style={{ width: "30px", height: "30px", border: "2px solid #ffcd32", borderRadius: "50%" }} />
                                </button>
                                <button onClick={this.broadcastFn.bind(this)} >
                                    <Icon type={this.state.iconStyle.bofang} style={{ width: "40px", height: "40px" }} />
                                </button>
                                <button onClick={this.downFn.bind(this)}>
                                    <Icon type="right" style={{ width: "30px", height: "30px", border: "2px solid #ffcd32", borderRadius: "50%" }} />
                                </button>
                                <button onClick={this.xihuanFn.bind(this)}>
                                    <Icon type={this.state.iconStyle.xihuan} style={{ width: "30px", height: "30px" }} />
                                </button>

                            </div>
                            <audio ref="audio" src={`http://aqqmusic.tc.qq.com/amobile.music.tc.qq.com/C400${this.props.isShowBfyData.bfyData.strMediaMid}.m4a?guid=1380654789&uin=0&fromtag=38`} autoplay="autoplay">
                            </audio>
                        </div>
                    </div>
                </div>
                <Foot />
            </BroadcastDiv>

        )
    }
    componentDidMount() {
        let audio = this.refs.audio
        console.log(audio)

    }
}

// 高阶组件
const mapStateToProps = state => {    //state为仓库数据

    return {
        isShowBfyData: state.toJS().bfy,
        singerData:state.toJS().bfy.bfyData.singer || []
    }
}

const mapDispatchToProps = dispatch => {
    return {
        changerIsShowBfyFn: params => dispatch(isShowBfy(params)),
        changeIsShowFootFn: params => dispatch(isShowFootAction(params))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Broadcast)
