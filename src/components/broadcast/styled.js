import styled from 'styled-components'

export const BroadcastDiv = styled.div`
    
    width:100%;
    height:100%;
    .gen{
        width:100%;
        height:100%;
        position:absolute;
        top:0px;
        left:0px;
        z-index:5;
        background:#222;
    }
   
    .bj{
        width:100%;
        height:100%;
        background:url("https://y.gtimg.cn/music/photo_new/T002R300x300M000003y8dsH2wBHlo.jpg?max_age=2592000") no-repeat;
        background-size: 100% 100%;
        filter: blur(25px) brightness(70%);
        float:left;
    }
    .main{
        width:100%;
        height:100%;
        position:absolute;
        align-items: center;
        .am-icon-down{
            position:fixed;
            color:#ffcd32;
        }
        .header{
            width:100%;
            height:60px;
            padding:0 10px;
            margin-bottom:25px;
           
               
                span{
                    text-align:center;
                    color:#fff;
                    width:100%;
                    line-height:40px;
                    display:inline-block;
                    font-size:18px;
                }
            
            
            h2{
                font-size:14px;
                width:100%;
                height:20px;
                text-align:center;
                color:#fff;
            }
        }
        .cd{
            width:100%;
            height:4.17rem;
            margin-bottom:20px;
            .cdRoll{
                width:3rem;
                height:3rem;
                margin:0 auto;
                border-radius:50%;
                
                img{
                    width:3rem;
                    height:3rem;
                    border-radius:50%;
                     border:10px solid hsla(0,0%,100%,.1);
                }
            }
            .cdRun{
                // 动画
                animation: imgrotate 20s linear infinite;}
            }
            .songWord{
                height: 20px;
                line-height: 20px;
                font-size: 14px;
                color: hsla(0,0%,100%,.5);
                text-align:center;
                margin-top:30px;
            }

            .am-carousel-wrap{
                position:relative;
                top:18px;
            }
        }
        .controls{
            height:98px;
            width:100%;
            padding-top:20px;
            span{
                color:#fff;
            }
            .btns{
                width:100%;
                height:40px;
                justify-content:space-around;
                display:flex;
                align-items: center;
                button{
                    
                    background:transparent;
                    color:#ffcd32;
                }
            }
        }
        
    }
    @keyframes imgrotate
    {       
        0%{transform:rotate(0deg);}
        50%{transform:rotate(180deg);}
        100%{transform:rotate(360deg);}
    }
`