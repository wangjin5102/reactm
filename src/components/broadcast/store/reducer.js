import {fromJS} from 'immutable'
import * as constants from './actionTypes'

const broadcastState = fromJS({
    isShowBfy: false,
    bfyData:{}
})
export default (state = broadcastState, action) => {
    switch(action.type)
    {
        case constants.ISSHOWBFY:
            return state.set('isShowBfy', action.data)
        case constants.BFYDATA:
            return state.set('bfyData', action.data)
        default:
            return state
    }
}
