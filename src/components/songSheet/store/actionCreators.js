// 常量
import * as constants from './actionTypes'

// 导入接口
import { 
    getSongSheetApi
} from "../../../api"


// 轮播图信息列表
export const getSongSheetAction = params => dispatch => {
    return getSongSheetApi(params).then(res => {
        dispatch({
            type: constants.GET_SONGSHEET,
            payload:res.data.data.list
        })
    })
}



