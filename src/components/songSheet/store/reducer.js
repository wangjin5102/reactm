import {fromJS} from 'immutable'
import * as constants from './actionTypes'

const songSheetState = fromJS({
    songSheet:[]
})
export default (state = songSheetState, action) => {
    switch(action.type)
    {
        case constants.GET_SONGSHEET:       
            return state.set('songSheet', fromJS(action.payload))
        default:
            return state
    }
}
