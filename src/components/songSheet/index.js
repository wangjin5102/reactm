import React, { Component } from 'react'

import { SongSheetDiv } from './styled'
// import { useParams } from 'react-router-dom'
// 导入antdm和动画
import { Icon } from 'antd-mobile';
import "animate.css";
import ReactCSSTransitionGroup from "react-addons-css-transition-group";

import { connect } from 'react-redux';

// 导入仓库数据
import {
    getSongSheetAction,
} from "./store/actionCreators"
import { isShowBfy, bfyDataAction} from '../broadcast/store/actionCreators'
// 导入第三方模块
import BScroll from "better-scroll"
class SongSheet extends Component {
    constructor(props) {
        super(props)
        this.state = {
        }
    }
    
    showBfyFn(value) {
        console.log(value)
        this.props.changerIsShowBfyFn(true)
        this.props.sendBfyDataFn(value)
    }
    render() {

        return (
            <ReactCSSTransitionGroup
                transitionEnter={true}
                transitionLeave={true}
                transitionEnterTimeout={1500}
                transitionLeaveTimeout={1500}
                transitionName="animated"
            >
                <SongSheetDiv className="animated fadeInRightBig" style={this.props.status.isShowSongSheet ? { display: 'block' } : { display: 'none' }}>
                    <div className="fanhui"  >
                        <button onClick={() => {
                            this.props.getSongSheetParamsFn()
                        }}>
                            <Icon type="left" style={{ fontSize: "22px" }} onClick={() => {
                                this.props.getSongSheetParamsFn()
                            }}/>
                        </button>
                        <span>{this.props.status.sonApiData.Fsinger_name}</span>
                    </div>
                    <div className="songSheet">
                        <div>

                            <div className="bj" style={{ background: `URL(https://y.gtimg.cn/music/photo_new/T001R300x300M000${this.props.status.sonApiData.Fsinger_mid}.jpg?max_age=2592000) no-repeat`, }}>
                            </div>
                            <ul>
                                {
                                    this.props.songSheetData.map((item, index) => {
                                        
                                        return <li
                                            key={index}
                                            onClick={this.showBfyFn.bind(this, item.musicData)}
                                        >
                                            <h1>
                                                {item.musicData.songname}
                                            </h1>
                                            <p>
                                                {item.musicData.songname}·
                                        {item.musicData.albumname}
                                            </p>
                                        </li>
                                    })
                                }
                            </ul>
                        </div>
                    </div>
                </SongSheetDiv>
            </ReactCSSTransitionGroup>
        )
    }
    componentDidMount() {
        let songSheet = document.querySelector(".songSheet")
        let scroll = new BScroll(songSheet, {
            click: true,
            probeType: 3,
        })

    }
}

// 高阶组件
const mapStateToProps = state => {    //state为仓库数据
    return {
        // musicData: state.toJS().recommend.music
        // singerData: state.toJS().singer.singer
        songSheetData: state.toJS().songsheet.songSheet
    }
}

const mapDispatchToProps = dispatch => {
    return {
        // musicFn: () => dispatch(getMusicAction()),
        songSheetFn: params => dispatch(getSongSheetAction(params)),
        changerIsShowBfyFn: params => dispatch(isShowBfy(params)),
        sendBfyDataFn: params => dispatch(bfyDataAction(params)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SongSheet)