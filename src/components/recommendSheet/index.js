import React, { Component } from 'react'

import { RecommendSheetDiv } from './styled'
// import { useParams } from 'react-router-dom'
// 导入antdm和动画
import { Icon } from 'antd-mobile';
import "animate.css";
import ReactCSSTransitionGroup from "react-addons-css-transition-group";

import { connect } from 'react-redux';

// 导入仓库数据

import { isShowBfy, bfyDataAction } from '../broadcast/store/actionCreators'
// 导入第三方模块
import BScroll from "better-scroll"
class RecommentSheet extends Component {
    constructor(props) {
        super(props)
        this.state = {
        }
    }

    showBfyFn(value) {
        this.props.changerIsShowBfyFn(true)
        this.props.sendBfyDataFn(value)
    }
    render() {
        console.log(this.props.recommentSheetData)
        return (
            <ReactCSSTransitionGroup
                transitionEnter={true}
                transitionLeave={true}
                transitionEnterTimeout={1500}
                transitionLeaveTimeout={1500}
                transitionName="animated"
            >
                <RecommendSheetDiv className="animated fadeInRightBig" style={this.props.status.isShowRecommentSheet ? { display: 'block' } : { display: 'none' }}>
                    <div className="fanhui"  >
                        <button onClick={() => {
                            this.props.getRecommentSheetParamsFn()
                        }}>
                            <Icon type="left" style={{ fontSize: "22px" }}  />
                        </button>
                        <span>{this.props.status.sonApiData.dissname}</span>
                    </div>
                    <div className="recommendSheet">
                        <div>

                            <div className="bj" style={{ background: `URL(${this.props.status.sonApiData.imgurl}) no-repeat`, }}>
                            </div>
                            <ul>
                                {
                                    this.props.recommentSheetData.map((item, index) => {
                                        return item.songlist.map((item, index) => { 
                                            console.log(item)
                                            return <li
                                                key={index}
                                                onClick={this.showBfyFn.bind(this,item)}
                                            >
                                                <h2>
                                                    {item.songname}
                                                </h2>
                                                <p>
                                                    {item.songname}·
                                                    {item.albumname}
                                                </p>
                                            </li>
                                        })
                                       
                                    })
                                }
                            </ul>
                        </div>
                    </div>
                </RecommendSheetDiv>
            </ReactCSSTransitionGroup>
        )
    }
    componentDidMount() {
        let recommendSheet = document.querySelector(".recommendSheet")
        let scroll = new BScroll(recommendSheet, {
            click: true,
            probeType: 3,
        })

    }
}

// 高阶组件
const mapStateToProps = state => {    //state为仓库数据
    console.log(state.toJS().recommend.recommentSheetData)
    return {
        // musicData: state.toJS().recommend.music
        // singerData: state.toJS().singer.singer
        recommentSheetData: state.toJS().recommend.recommentSheetData
    }
}

const mapDispatchToProps = dispatch => {
    return {
        // musicFn: () => dispatch(getMusicAction()),
        
        changerIsShowBfyFn: params => dispatch(isShowBfy(params)),
        sendBfyDataFn: params => dispatch(bfyDataAction(params)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(RecommentSheet)