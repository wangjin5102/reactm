import styled from 'styled-components'

export const RecommendSheetDiv = styled.div`
    width: 100%;
    height: 100%;
    background: #222;
    position: absolute;
    left: 0px;
    top: 0px;
    z-index: 5;
    overflow:hidden;
    .fanhui{
        width:100%;
        height:40px;
        position:fixed;
        top:0;
        left:0;
        z-index:14;
        overflow:hidden;
        button{
            margin-left:10px;
            margin-top:8px;
            background:transparent;
            color: #ffcd32;
            width:22px;
            height:22px;
        }
        span{
            display:inline-block;
            color:#fff;
            font-size:18px;
            width:80%;
            text-align:center;
            bottom:5px;
            left:5px;
            position:relative;
        }
    }
    .recommendSheet{
        width: 100%;
        height: 100%;
        overflow:hidden;
        .bj{
            padding-top:70%;
            width:100%;
            height:0;
            background-size:cover !important;
            
        }
        
        ul{
            width:90%;
            margin:auto;
            li{
                display:flex;
                flex-direction:column;
                justify-content: center;
                padding:0;
                height:64px;
                h2{
                    
                    padding-top:5px;
                    font-size:14px;
                    color:#fff;
                }
                p{
                    width:100%;
                    height:20px;
                    overflow:hidden;
                    text-overflow:ellipsis;
                      white-space: nowrap;
                    padding-top:4px;
                    font-size:14px;
                    color: hsla(0,0%,100%,.3); 
                }
            }
        }
    }
    
   
`