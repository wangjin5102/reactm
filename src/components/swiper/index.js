import React, { Component } from 'react'
import { connect } from 'react-redux';

// 导入仓库数据
import {
    getLbtAction,
} from "../../pages/recommend/store/actionCreators"
// styled样式

import { SwiperDiv } from './styled'
// 轮播图
import Swiper from 'swiper/js/swiper.js'
import 'swiper/css/swiper.min.css'
class Lbt extends Component {
    render() {
        console.log(this.props.lbtData)
        return (
            <SwiperDiv>
                <div className="swiper-container">
                    <div className="swiper-wrapper">
                        {/* <div className="swiper-slide">Slide 1</div>
                        <div className="swiper-slide">Slide 2</div>
                        <div className="swiper-slide">Slide 3</div>
                        <div className="swiper-slide">Slide 5</div> */}
                        {
                            this.props.lbtData.map((item, index) => { 
                                return <div className="swiper-slide" key={index}>
                                    <img src={item.picUrl} alt={index}/>
                                </div>
                            })
                        }
                    </div>
                    <div className="swiper-pagination"></div>
                </div>
            </SwiperDiv>
        )
    }
    initLbt() { 
        new Swiper('.swiper-container', {
            loop: true,  //循环
            autoplay: {   //滑动后继续播放（不写官方默认暂停）
                disableOnInteraction: false,
            },
            pagination: {  //分页器
                el: '.swiper-pagination'
            }
        })
    }
    componentDidUpdate() { 
        this.initLbt()
    }
    componentDidMount() { 
        // 异步请求
        this.props.lbtFn()
        this.initLbt()
    }
}
// 高阶组件
const mapStateToProps = state => {    //state为仓库数据
    console.log(state.toJS())
    return {
        lbtData: state.toJS().recommend.lbt,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        lbtFn: () => dispatch(getLbtAction()),


    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Lbt)
