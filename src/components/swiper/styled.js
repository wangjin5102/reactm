import styled from 'styled-components'

export const SwiperDiv = styled.div`
    width:100%;
    height:1.5rem;
    background:green;
    .swiper-container {
        width:100%;
        height:1.5rem;
        .swiper-pagination{
            bottom:-.2rem;
        }
        img{
            width:100%;
            height:100%;
        }
    }  
`