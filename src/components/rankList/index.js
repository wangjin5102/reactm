import React, { Component } from 'react'
import { connect } from 'react-redux';
import { RankListDiv } from './styled'
// import { useParams } from 'react-router-dom'
// 导入antdm和动画
import { Icon } from 'antd-mobile';
import "animate.css";
import ReactCSSTransitionGroup from "react-addons-css-transition-group";
// 导入第三方模块
import BScroll from "better-scroll"
// 导入仓库数据

import { isShowBfy, bfyDataAction } from '../broadcast/store/actionCreators'
class RankList extends Component {
    constructor(props) {
        super(props)
        this.state = {
        }
    }
    // 跳转播放页
    showBfyFn(value) {
        this.props.changerIsShowBfyFn(true)
        this.props.sendBfyDataFn(value)
    }
    render() {
        console.log(this.props.status.sonApiData)
        
        return (
            <ReactCSSTransitionGroup
                transitionEnter={true}
                transitionLeave={true}
                transitionEnterTimeout={1500}
                transitionLeaveTimeout={1500}
                transitionName="animated"
            >
                <RankListDiv className="animated fadeInRightBig" style={this.props.status.isShowRankList ? { display: 'block' } : { display: 'none' }}>
                   
                    <div className="fanhui"  >
                        <button onClick={() => {
                            this.props.getRankListParamsFn()
                        }}>
                            <Icon type="left" style={{ fontSize: "22px" }} />
                        </button>
                        <span>{this.props.status.sonApiData.topTitle}</span>
                    </div>
                    <div className="rankList" >
                        <div>

                            <div className="bj" style={{ background: `URL(${this.props.status.sonApiData.picUrl})`}}>
                            </div>
                            <ul>
                                {
                                    this.props.rankListData.map((item, index) => {
                                        console.log(item.data)
                                        return <li
                                            key={index}
                                            onClick={this.showBfyFn.bind(this,item.data)}
                                        >
                                            <h2>
                                                {item.data.songname}
                                            </h2>
                                            <p>
                                                {item.data.singer[0].name}-
                                                    {item.data.albumname}
                                            </p>
                                        </li>

                                    })
                                }
                            </ul>
                        </div>
                    </div>
                </RankListDiv>
            </ReactCSSTransitionGroup>
        )
    }
    componentDidMount() { 
        let rankList = document.querySelector(".rankList")
        let scroll = new BScroll(rankList, {
            click: true,
            probeType: 3,
        })

    }
}

// 高阶组件
const mapStateToProps = state => {    //state为仓库数据
    console.log(state.toJS().rank.rankList)
    return {
        // musicData: state.toJS().recommend.music
        rankListData: state.toJS().rank.rankList
    }
}

const mapDispatchToProps = dispatch => {
    return {
      
        changerIsShowBfyFn: params => dispatch(isShowBfy(params)),
        sendBfyDataFn: params => dispatch(bfyDataAction(params))

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(RankList)