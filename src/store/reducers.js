import { combineReducers} from "redux-immutable"

import { reducer as BfyReducer} from '../components/broadcast/store'
import { reducer as RecommendReducer} from '../pages/recommend/store'
import { reducer as SingerReducer} from '../pages/singer/store'
import { reducer as SongSheetReducer} from '../components/songSheet/store'
import { reducer as RankReducer} from '../pages/rank/store'
import { reducer as FootReducer} from '../components/foot/store'


const reducers = combineReducers({
    bfy: BfyReducer,
    recommend: RecommendReducer,
    singer: SingerReducer,
    songsheet:SongSheetReducer,
    rank: RankReducer,
    foot:FootReducer,
})

export default reducers