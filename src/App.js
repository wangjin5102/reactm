// 导入库
import React, { Component } from 'react'

// 导入样式
import './static/css/reset.less';
import { AppDiv} from './styled'

// 导入路由
import Router from './router/index'

// 导入组件
import Broadcast from "./components/broadcast"
// 定义组件
class App extends Component {
    render() {
        return (
            <AppDiv>
                <Router />
                <Broadcast />
            </AppDiv>
        )
    }
}

export default App