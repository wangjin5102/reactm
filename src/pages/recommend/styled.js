import styled from 'styled-components'

export const RecommendDiv = styled.div`
    width:100%;
    height:100%;
    display:flex;
    flex-direction:column;
    .recommend{
        flex:1;
        width:90%;
        margin:auto;
        overflow:hidden;
        h1{
            height: 65px;
            line-height: 65px;
            text-align: center;
            font-size: 14px;
            color: #ffcd32;
            margin:0px;
        }
    }
    ul{
        
        li{
            display:flex;
            padding-bottom:20px;
            .l{
                width:80px;
                height:60px;
                display:flex;
                img{
                    width:60px;
                    height:60px;
                }
            }
            .r{
                .title{
                    font-size:18px;
                    line-height:1.5;
                    color:#fff;
                }
                .desc{
                    font-size:14px;
                    line-height:2;
                    color: hsla(0,0%,100%,.3);
                }
            }
        }
    }
`