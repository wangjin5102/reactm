import React, { Component } from 'react'
import { connect } from 'react-redux';

// 导入仓库数据
import {
    getMusicAction,
    getRecommentSheetAction,
} from "./store/actionCreators"
// 导入styled 样式
import { RecommendDiv } from "./styled"
// 公共头部
import Header from "../../components/header"
import Navs from "../../components/navs"
import Lbt from "../../components/swiper"
// 引入子组件
import RecommentSheet from "../../components/recommendSheet"
// 导入第三方模块
import BScroll from 'better-scroll'

class Recommend extends Component {
    constructor(props) {
        super(props)
        this.state = {
            sonData: {
                isShowRecommentSheet: false,
                sonApiData: []
            },
        }
    }
    // 转到详情页
    showRecommentSheetFn(value) {
        this.setState({
            sonData: {
                isShowRecommentSheet: true,
                sonApiData: value
            },
        })
        console.log(value)
        this.props.recommendSheetFn(value)
        
        
    }
    // 
    getRecommentSheetParamsFn = () => {

        this.setState({
            sonData: {
                isShowRecommentSheet: false,
                sonApiData: []
            },
        })
    }
   
    render() {
        console.log(this.props.musicData)
        return (
            <RecommendDiv>
                <Header />
                <Navs />
                <Lbt />
                <RecommentSheet status={this.state.sonData} getRecommentSheetParamsFn={this.getRecommentSheetParamsFn}/>
                <div className="recommend">
                    <div>
                        <h1 onClick={() => { console.log(1)}}>热门歌单推荐</h1>
                    <ul>
                        {
                            this.props.musicData.map((item, index) => {
                                return (
                                    <li key={index} onClick={this.showRecommentSheetFn.bind(this, item)}>
                                        <div className="l">
                                            <img src={item.imgurl} alt={item.creator.name}></img>
                                        </div>
                                        <div className="r">
                                            <p className="title">{item.creator.name}</p>
                                            <p className="desc">{item.dissname}</p>
                                        </div>
                                    </li>
                                )
                            })
                        }
                        </ul>
                    </div>
                </div>
            </RecommendDiv>
        )
    }
    componentDidMount() {
        // 异步请求
        this.props.musicFn()
        // 滑动
        let recommend = document.querySelector(".recommend")
        let scroll = new BScroll(recommend, {
            click: true,
            pullUpLoad: {
                
                threshold: 50
            }
        })
    }
}


// 高阶组件
const mapStateToProps = state => {    //state为仓库数据
    return {
        musicData: state.toJS().recommend.music
    }
}

const mapDispatchToProps = dispatch => {
    return {
        musicFn: () => dispatch(getMusicAction()),
        recommendSheetFn: params => dispatch(getRecommentSheetAction(params))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Recommend)