// 常量
import * as constants from './actionTypes'

// 导入接口
import { 
    getLbtApi,
    getMusicApi,
    getRecommentSheetApi
} from "../../../api"


// 轮播图信息列表
export const getLbtAction = () => dispatch => {
    return getLbtApi().then(res => {
        dispatch({
            type: constants.GET_LBT,
            payload:res.data.slider
        })
    })
}

// 推荐页信息列表
export const getMusicAction = () => dispatch => {
    return getMusicApi().then(res => {
        dispatch({
            type: constants.GET_MUSIC,
            payload: res.data.list
        })
    })
}
// 推荐页详情列表
export const getRecommentSheetAction = params => dispatch => {
    return getRecommentSheetApi(params).then(res => {
        console.log(res)
        dispatch({
            type: constants.GET_RECOMMENT,
            payload: res.cdlist
        })
    })
}


