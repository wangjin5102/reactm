import {fromJS} from 'immutable'
import * as constants from './actionTypes'

const singerListState = fromJS({
    lbt: [],
    music: [],
    recommentSheetData:[],
})
export default (state = singerListState, action) => {
    switch(action.type)
    {
        case constants.GET_LBT:       
            return state.set('lbt', fromJS(action.payload))
        case constants.GET_MUSIC:
            return state.set('music', fromJS(action.payload))
        case constants.GET_RECOMMENT:
            console.log(action.payload)
            return state.set('recommentSheetData', fromJS(action.payload))
        default:
            return state
    }
}
