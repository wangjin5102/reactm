import React, { Component } from 'react'

import { connect } from 'react-redux';
// 引入仓库数据
import {
    isShowBfy
} from '../../components/broadcast/store/actionCreators'
// 导入样式
import { SearchDiv } from './styled'
// 公共头部、导航
import Header from "../../components/header"
import Navs from "../../components/navs"
// 导入第三方
import { Icon } from 'antd-mobile';
class Search extends Component {
    constructor(props) { 
        super(props)
        this.state = {
            isShowBtn: false,
            searchValue: "",
            hotSearch:["我们的歌","冰雪奇缘2","张杰","桥边姑娘","星辰大海"],
        }
    }
    // 搜索框数据
    changeFn() { 
        let value = document.querySelector(".search input").value
        console.log(value)
        this.setState({
            isShowBtn: true,
            searchValue:value,
        })
    }
    // 点击×清楚input值
    clearFn() { 

        this.setState({
            isShowBtn:false,
            searchValue: "",
        })
    }
    a() { 
        this.props.changerIsShowBfyFn(true)
        console.log(this.props.isShowBfyData.isShowBfy)
    }
    render() {
        return (
            <SearchDiv>
                <Header />
                <Navs />
                <div className="main">
                    <div className="search">
                        <Icon type="search" style={{color:"#222"}}/>
                        <input
                            type="text"
                            placeholder="搜索歌曲、歌手"
                            onChange={this.changeFn.bind(this)}
                            value={this.state.searchValue}
                        />
                        <button style={this.state.isShowBtn ? {display:"block"} : {display:"none"}} onClick={this.clearFn.bind(this)}><Icon type="cross-circle-o" size="xxs"/></button>
                    </div>
                    <div className="hot">
                        <h1 onClick={this.a.bind(this)}>热门搜索</h1>
                        <ul>
                            {this.state.hotSearch.map((item,index) => {
                                return <li key={index}>{item}</li>
                            })}
                        </ul>
                    </div>
                </div>
            </SearchDiv>
        )
    }
}

// export default Search
// 高阶组件
const mapStateToProps = state => {    //state为仓库数据

    return {
        isShowBfyData: state.toJS().bfy
    }
}

const mapDispatchToProps = dispatch => {
    return {
       
        changerIsShowBfyFn: params => dispatch(isShowBfy(params))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Search)