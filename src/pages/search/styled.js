import styled from 'styled-components'

export const SearchDiv = styled.div`
    width:100%;
    height:100%;
    .main{
        width:90%;
        height:100%;
        margin:auto;
        .search{
            height:40px;
            width:100%;
            background: #333;
            border-radius: 6px;
            margin:20px auto;
            display:flex;
            align-items:center;
            input{
                padding-left:10px;
                height:100%;
                border:none;
                flex:1;
                background: #333;
                color:#fff;
            }
            button{
                width:16px;
                height:16px;
                background: #333;
                margin-right:5px;
            }
        }
        .hot{
            font-size:14px;
            color: hsla(0,0%,100%,.5);
            h1{
                font-size:14px;
                margin-bottom:20px;
            }
            ul{
                display:flex;
                flex-wrap: wrap;
                li{
                    padding: 5px 10px;
                    margin: 0 20px 10px 0;
                    border-radius: 6px;
                    background: #333;

                    color: hsla(0,0%,100%,.3);

                }
            }
        }
    }
`