import styled from 'styled-components'

export const SingerDiv = styled.div`
    width:100%;
    height:100%;
    display:flex;
    flex-direction:column;
    .singerIndex{
        width:20px;
        background:rgba(0,0,0,.3);
        border-radius:10px;
        font-family:Helvetica;
        position:fixed;
        right:0px;
        top:50%;
        transform:translateY(-41%);
        z-index:1;
        li{
            width:100%;
            text-align:center;
            font-size:12px;
            color:hsla(0,0%,100%,.5);
            padding:2px 0;
        }
        li.active{
            color:#ffcd32;
        }
    }
    .singers{
        width:100%;
        flex:1;
        overflow:hidden;
        .singer{
            
            color:hsla(0,0%,100%,.5);
            h2{
                width:100%;
                height:30px;
                line-height:30px;
                padding-left:10px;
                font-size:16px;
                background:#333;
            }
            ul{
                width:96%;
                margin:auto;
                li{
                    width:100%;
                    height:70px;
                    display:flex;
                    align-items:center;
                    img{
                        width:50px;
                        height:50px;
                        border-radius:50%;
                    }
                    span{
                        padding-left:20px;
                        font-size:14px;
                    }
                }
            }
        }
    }
`