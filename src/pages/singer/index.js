import React, { Component } from 'react'
import { connect } from 'react-redux';

// 导入仓库数据
import {
    getSingerAction,
} from "./store/actionCreators"
import { 
    getSongSheetAction,
} from "../../components/songSheet/store/actionCreators"
// 导入样式
import { SingerDiv } from './styled'
// 公共头部
import Header from "../../components/header"
import Navs from "../../components/navs"
import SongSheet from "../../components/songSheet"
// 导入第三方模块
import BScroll from "better-scroll"
class Singer extends Component {
    constructor(props) {
        super(props)
        this.state = {

            sonData: {
                isShowSongSheet: false,
                sonApiData: {}
            },
            scroll: null,
            index: ["热", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"],
            currentIndex: 0,

        }
    }
    goListFn(item) {
        console.log(this.refs, item)
        item = item === "热" ? "热门" : item
        this.state.scroll.scrollToElement(this.refs[item], 1000)
    }

    // 转到详情页
    showSongSheetFn(value) {
        this.setState({
            sonData: {
                isShowSongSheet: true,
                sonApiData: value
            },
        })
        
        this.props.songSheetFn(value)
    }
    // 
    getSongSheetParamsFn = () => {
        
        this.setState({
            sonData: {
                isShowSongSheet: false,
                sonApiData:[]
            },
        })
    }
    render() {
        return (
            <SingerDiv>
                <Header />
                <Navs />
                <SongSheet status={this.state.sonData} getSongSheetParamsFn={this.getSongSheetParamsFn} />
                {/* 右边导航 */}
                <ul className="singerIndex">
                    {this.state.index.map((item, index) => {
                        return <li
                            key={index}
                            className={this.state.currentIndex === index ? "active" : ""}
                            onClick={this.goListFn.bind(this, item)}
                        >
                            {item}
                        </li>
                    })}
                </ul>
                {/* 右边导航 */}
                <div className="singers">
                    <div>
                        {this.props.singerData.map((item, index) => {
                            return <div
                                className="singer"
                                key={index}
                                ref={item.title}
                            >
                                <h2>{item.title}</h2>
                                <ul>
                                    {
                                        item.starts.map((item, index) => {
                                            return <li key={index} onClick={this.showSongSheetFn.bind(this, item)}>
                                                <img src={`https://y.gtimg.cn/music/photo_new/T001R300x300M000${item.Fsinger_mid}.jpg?max_age=2592000`} alt={item.name} />
                                                <span>{item.Fsinger_name}</span>
                                            </li>
                                        })
                                    }
                                </ul>
                            </div>
                        })}
                    </div>
                </div>
            </SingerDiv>
        )
    }
    componentDidMount() {

        // 异步请求
        this.props.singerFn()
        let singers = document.querySelector(".singers")
        let scroll = new BScroll(singers, {
            click: true,
            probeType: 3,
        })

        this.setState({ scroll })
        // 过滤this.refs

        let h2ScrollTop = []
        for (let key in this.refs) {
            console.log(key, this.refs[key])
            h2ScrollTop.push(this.refs[key].offsetTop - 88)
        }
        //监控滚动
        console.log(this.refs)
        scroll.on("scroll", (pos) => {
            // scroll.finishPullUp()
            let y = Math.abs(pos.y)
            for (let i = h2ScrollTop.length - 1; i >= 0; i--) {
                if (y >= h2ScrollTop[i]) {
                    this.setState({
                        currentIndex: i
                    })
                    break;
                }
            }
        })

    }
    componentDidUpdate() {
        console.log(this.refs)
    }
}

// 高阶组件
const mapStateToProps = state => {    //state为仓库数据
    console.log(state.toJS())
    return {
        // musicData: state.toJS().recommend.music
        singerData: state.toJS().singer.singer
    }
}

const mapDispatchToProps = dispatch => {
    return {
        // musicFn: () => dispatch(getMusicAction()),
        singerFn: () => dispatch(getSingerAction()),
        songSheetFn: params => dispatch(getSongSheetAction(params))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Singer)