import { fromJS } from 'immutable'
import * as constants from './actionTypes'

const singerListState = fromJS({
    singer: []
})
export default (state = singerListState, action) => {
    switch (action.type) {
        case constants.GET_SINGER:

            let singers = [
                {title: "热门",starts: action.payload.slice(0, 10)},
                {title: "A",starts:[]},
                {title: "B",starts:[]},
                {title: "C",starts:[]},
                {title: "E",starts:[]},
                {title: "F",starts:[]},
                {title: "G",starts:[]},
                {title: "H",starts:[]},
                {title: "J",starts:[]},
                {title: "K",starts:[]},
                {title: "L",starts:[]},
                {title: "M",starts:[]},
                {title: "N",starts:[]},
                {title: "O",starts:[]},
                {title: "P",starts:[]},
                {title: "Q",starts:[]},
                {title: "R",starts:[]},
                {title: "S",starts:[]},
                {title: "T",starts:[]},
                {title: "W",starts:[]},
                {title: "X",starts:[]},
                {title: "Y",starts:[]},
                {title: "Z",starts:[]}
            ]
            
            
            for (let i = 0; i < action.payload.length; i++) {
                for (let v = 0; v < singers.length; v++) { 
                    if (action.payload[i].Findex === singers[v].title) { 
                        singers[v].starts.push(action.payload[i])
                    }
                }
            }
            return state.set('singer', singers)
        default:
            return state
    }
}
