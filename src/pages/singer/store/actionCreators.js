// 常量
import * as constants from './actionTypes'
// 导入接口
import {
    getSingerApi
} from "../../../api"


// 歌手页信息列表
export const getSingerAction = () => dispatch => {
    return getSingerApi().then(res => {
        console.log(res)
        dispatch({
            type: constants.GET_SINGER,
            payload: res.data.list
        })
    })
}


