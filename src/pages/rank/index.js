import React, { Component } from 'react'
// 导入样式
import { RankDiv } from './styled'
// 公共头部
import Header from "../../components/header"
import Navs from "../../components/navs"
import RankList from "../../components/rankList"
// 导入第三方模块
import BScroll from "better-scroll"
// 导入仓库数据
import { connect } from 'react-redux';
import { getRankAction, getRankListAction} from "./store/actionCreators"
class Rank extends Component {
    constructor(props) {
        super(props)
        this.state = {
            sonData: {
                sonApiData: [],
                isShowRankList:false
            }
        }
    }
    // 转到详情页
    showRankListFn(value) {
        this.setState({
            sonData: {
                sonApiData: value,
                isShowRankList: true
            }
        })
        this.props.rankListDataFn(value)
    }
    getRankListParamsFn = () => {
        this.setState({
            sonData: {
                sonApiData: [],
                isShowRankList: false
            }
        })
    }
    render() {
        console.log(this.props.rankData)
        return (
            <RankDiv>
                <Header />
                <Navs />
                {/* 排行分类列表 */}
                <RankList status={this.state.sonData} getRankListParamsFn={this.getRankListParamsFn} />
                <div className="main">
                    <div>
                        <ul>
                            {this.props.rankData.map((item, index) => {
                                return <li key={index} onClick={this.showRankListFn.bind(this,item)}>
                                    <img src={item.picUrl} alt="加载中"/>
                                    <div>
                                        {
                                            item.songList.map((item, index) => { 
                                                return <p key={index}>{item.songname}-{item.singername}</p>
                                            })
                                        }
                                    </div>
                                </li>
                            })}
                        </ul>
                    </div>
                </div>
            </RankDiv>
        )
    }
    componentDidMount() {
        // 异步请求
        this.props.rankDataFn()
        let main = document.querySelector(".main")
        let scroll = new BScroll(main, {
            click: true,
            probeType: 3,
        })

      

        //监控滚动
        // console.log(this.refs)
        scroll.on("scroll", (pos) => {
            
        })

    }
}


// 高阶组件
const mapStateToProps = state => {    //state为仓库数据
    console.log(state.toJS())
    return {
        // musicData: state.toJS().recommend.music
        rankData :state.toJS().rank.rank
    }
}

const mapDispatchToProps = dispatch => {
    return {
        // musicFn: () => dispatch(getMusicAction()),
        rankDataFn: () => dispatch(getRankAction()),
        rankListDataFn: params => dispatch(getRankListAction(params)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Rank)