import { fromJS } from 'immutable'
import * as constants from './actionTypes'

const rankState = fromJS({
    rank: [],
    rankList:[],
})
export default (state = rankState, action) => {
    switch (action.type) {
        case constants.GET_RANK:
            return state.set('rank', fromJS(action.payload))
        case constants.GET_RANKLIST:
            return state.set('rankList', fromJS(action.payload.songlist))
        default:
            return state
    }
}
