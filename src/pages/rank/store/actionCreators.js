// 常量
import * as constants from './actionTypes'
// 导入接口
import {
    getRankApi,
    getRankListApi
} from "../../../api"


// 排行信息列表
export const getRankAction = () => dispatch => {
    return getRankApi().then(res => {
        console.log(res)
        dispatch({
            type: constants.GET_RANK,
            payload: res.data.topList
        })
    })
}
// 排行信息列表
export const getRankListAction = params => dispatch => {
    return getRankListApi(params).then(res => {
        console.log(res)
        dispatch({
            type: constants.GET_RANKLIST,
            payload: res.data
        })
    })
}

