import styled from 'styled-components'

export const RankDiv = styled.div`
    width:100%;
    height:100%;
    .main{
        width:100%;
        height:100%;
        overflow:hidden;
        ul{
            width:90%;
            margin:auto;
            display:flex;
            flex-direction: column;
            li{
                display:flex;
                margin-top:20px;
                height:100px;

                img{
                    width:100px;
                    height:100px;
                }
                div{
                    font-size:12px;
                    height:100%;
                    line-height:26px;
                    color:rgba(255,255,255,.3);
                    padding:0px 20px;
                    display:flex;
                    flex-direction: column;
                    justify-content: center;
                }
            }
        }
    }
`