import request from './request'

// 函数名规则：HTTP动词功能Api 
//       目的：防止冲突
// 相关语法：
// request.get('请求路径', {headers,..,params: 数据对象}).then(res => res.data)
// request.post('请求路径', 数据对象).then(res => res.data)
// request.put('请求路径', 数据对象).then(res => res.data)
// request.delete(`请求路径/${id}`).then(res => res.data)

// /**
//  * 登录接口
//  * @param {object} params {username,password}
//  */
// export const postLoginApi = params => {
//     return request.post('login', params).then(res => res.data)
// }

/**
 * 推荐页-轮播图接口
 * @param {} 
 */
export const getLbtApi = () => {
    return request.get('/api/music/api/getTopBanner?g_tk=1928093487&inCharset=utf8&outCharset=utf-8&notice=0&format=json&platform=yqq.json&hostUin=0&needNewCode=0&-=recom557284451453973&data=%7B%22comm%22:%7B%22ct%22:24%7D,%22category%22:%7B%22method%22:%22get_hot_category%22,%22param%22:%7B%22qq%22:%22%22%7D,%22module%22:%22music.web_category_svr%22%7D,%22recomPlaylist%22:%7B%22method%22:%22get_hot_recommend%22,%22param%22:%7B%22async%22:1,%22cmd%22:2%7D,%22module%22:%22playlist.HotRecommendServer%22%7D,%22playlist%22:%7B%22method%22:%22get_playlist_by_category%22,%22param%22:%7B%22id%22:8,%22curPage%22:1,%22size%22:40,%22order%22:5,%22titleid%22:8%7D,%22module%22:%22playlist.PlayListPlazaServer%22%7D,%22new_song%22:%7B%22module%22:%22newsong.NewSongServer%22,%22method%22:%22get_new_song_info%22,%22param%22:%7B%22type%22:5%7D%7D,%22new_album%22:%7B%22module%22:%22newalbum.NewAlbumServer%22,%22method%22:%22get_new_album_info%22,%22param%22:%7B%22area%22:1,%22sin%22:0,%22num%22:10%7D%7D,%22new_album_tag%22:%7B%22module%22:%22newalbum.NewAlbumServer%22,%22method%22:%22get_new_album_area%22,%22param%22:%7B%7D%7D,%22toplist%22:%7B%22module%22:%22musicToplist.ToplistInfoServer%22,%22method%22:%22GetAll%22,%22param%22:%7B%7D%7D,%22focus%22:%7B%22module%22:%22QQMusic.MusichallServer%22,%22method%22:%22GetFocus%22,%22param%22:%7B%7D%7D%7D').then(res => res.data)
}

/**
 * 推荐页-歌曲接口
 * @param {} 
 */

export const getMusicApi = () => {
    //http://ustbhuangyi.com/music/api/getDiscList?g_tk=1928093487&inCharset=utf-8&outCharset=utf-8&notice=0&format=json&platform=yqq&hostUin=0&sin=0&ein=29&sortId=5&needNewCode=0&categoryId=10000000&rnd=0.5679851703559871
    // return request.get('/api/').then(res => res.data)
    return request.get('/api/music/api/getDiscList?g_tk=1928093487&inCharset=utf-8&outCharset=utf-8&notice=0&format=json&platform=yqq&hostUin=0&sin=0&ein=29&sortId=5&needNewCode=0&categoryId=10000000&rnd=0.5679851703559871', {Headers :{ "X-XSRF-TOKEN": "ofeeCc5p - ZE2iUsN4aaeCLWWQ7wZPkWE1Y1c", "Cookie":"_csrf=z9V7h4FR-rYAJEUFDrryCc4P; XSRF-TOKEN=ofeeCc5p-ZE2iUsN4aaeCLWWQ7wZPkWE1Y1c"}}).then(res => res.data)
}
// 推荐详情页
export const getRecommentSheetApi = params => { 
    console.log(params)
    return request.get(`/api/music/api/getCdInfo?g_tk=1928093487&inCharset=utf-8&outCharset=utf-8&notice=0&format=json&disstid=${params.dissid}&type=1&json=1&utf8=1&onlysong=0&platform=yqq&hostUin=0&needNewCode=0`).then(res => res.data)
}

// 歌手页
export const getSingerApi = () => {
    
    return request.get('/adc/v8/fcg-bin/v8.fcg?g_tk=1928093487&inCharset=utf-8&outCharset=utf-8&notice=0&format=json&channel=singer&page=list&key=all_all_all&pagesize=100&pagenum=1&hostUin=0&needNewCode=0&platform=yqq&jsonCallback=jp0').then(res => res.data)
}

// 歌手详情页
export const getSongSheetApi = params => { 
    return request.get(`/adc/v8/fcg-bin/fcg_v8_singer_track_cp.fcg?g_tk=1928093487&inCharset=utf-8&outCharset=utf-8&notice=0&format=json&hostUin=0&needNewCode=0&platform=yqq&order=listen&begin=0&num=80&songstatus=1&singermid=${params.Fsinger_mid}&jsonCallback=jp${params.Fsort}`)
}

// 排行信息
export const getRankApi = () => {
    return request.get("/adc/v8/fcg-bin/fcg_myqq_toplist.fcg?g_tk=1928093487&inCharset=utf-8&outCharset=utf-8&notice=0&format=json&uin=0&needNewCode=1&platform=h5&jsonCallback=jp0").then(res => res.data)
}
// 排行详细信息
export const getRankListApi = params => {
    return request.get(`/adc/v8/fcg-bin/fcg_v8_toplist_cp.fcg?g_tk=1928093487&inCharset=utf-8&outCharset=utf-8&notice=0&format=json&topid=${params.id}&needNewCode=1&uin=0&tpl=3&page=detail&type=top&platform=h5&jsonCallback=jp1`)
}
